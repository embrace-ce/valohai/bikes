from pathlib import Path
from argparse import ArgumentParser

import pandas as pd
import models
import json


def get_hyper_parameters(args):
    args = args.__dict__
    possible_params = ["max_depth", "bootstrap", "n_estimators"]
    hyper_parameters = {p: args[p] for p in possible_params if args.get(p, None)}
    if len(hyper_parameters) > 0 and args["model"] != "RandomForest":
        raise NotImplementedError()
    return hyper_parameters


def run_experiment(args):
    counts = pd.read_csv(Path(args.data) / "counts" / "hourly_counts.csv", parse_dates=[0], index_col=0)[args.station]
    counts = counts.fillna(0)  # todo proper data imputation

    weather = pd.read_csv(Path(args.data) / "weather" / "berlin_2012_2018.csv", parse_dates=[0], index_col=0)
    weather = weather.reindex(counts.index)
    weather["apparentTemperature"] = weather["apparentTemperature"].fillna(0)  # todo proper data imputation
    weather["precipIntensity"] = weather["precipIntensity"].fillna(0)
    weather["windSpeed"] = weather["windSpeed"].fillna(0)

    split_date = pd.to_datetime(args.split)
    train = counts.index < split_date
    test = counts.index >= split_date

    model_params = get_hyper_parameters(args)

    return models.run_model(args.model, args.features, counts[train], counts[test], weather[train], weather[test], Path(args.results), model_params)


if __name__== "__main__":
    parser = ArgumentParser()
    parser.add_argument("model")
    parser.add_argument("features")
    parser.add_argument("--data", default="data/")
    parser.add_argument("--results", default="results/")
    parser.add_argument("--station", default="12-PA-SCH")
    parser.add_argument("--split", default="2018-01-01")
    # todo should be subparser?
    parser.add_argument("--max_depth", type=int)
    parser.add_argument("--bootstrap", type=bool)
    parser.add_argument("--n_estimators", type=int)

    args = parser.parse_args()
    results = run_experiment(args)
    print(json.dumps(results))
    assert False
