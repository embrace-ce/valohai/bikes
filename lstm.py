# started a new file because way model training was set up does not work well with tracking experiment over time
# should integrate  todo

from pathlib import Path
from argparse import ArgumentParser

import pandas as pd
from sklearn.preprocessing import MinMaxScaler
from keras.layers.core import Dense, Activation, Dropout
from keras.layers import Input
from keras.layers.recurrent import LSTM
from keras.models import Sequential, Model
import numpy as np
from sklearn.metrics import mean_squared_error
from keras.callbacks import Callback, TensorBoard
import json


def prepare_data(data_dir, station, split_date, num_timesteps):
    counts = pd.read_csv(data_dir / "counts" / "hourly_counts.csv", parse_dates=[0], index_col=0)[station]
    counts = counts.fillna(0)
    split_date = pd.to_datetime(split_date)
    train = counts.index < split_date

    scale = MinMaxScaler(feature_range=(-1, 1))
    scale.fit(counts[train].values.reshape(len(counts[train]), 1))  # fit scaler on train dataset
    scaled = scale.transform(counts.values.reshape(len(counts), 1))[:, 0]  # apply on full dataset
    counts = pd.Series(scaled, index=counts.index)

    features = [counts.shift(i) for i in range(1, num_timesteps + 1)]  # todo data leakage
    features = pd.concat(features, axis=1)
    feature_names = ["lag_{}".format(i) for i in range(num_timesteps)]
    features.columns = feature_names

    data = features
    data["target"] = counts.copy()
    data = data.dropna()  # due to calculating lags added some NaNs at beginning at dataset -> just drop

    # need to recalculate split indexes, since dropped a few rows in previous step
    train = data.index < split_date
    test = data.index >= split_date

    return scale, data[feature_names][train], data["target"][train], data[feature_names][test], data["target"][test]


def prepare_model(num_timesteps):
    sequential_input = Input(shape=(num_timesteps, 1))
    x = LSTM(10, return_sequences=True)(sequential_input)
    x = Dropout(0.5)(x)
    x = LSTM(10, return_sequences=False)(x)
    x = Dropout(0.5)(x)
    x = Dense(1)(x)
    m = Model(inputs=sequential_input, outputs=x)
    m.compile(loss='mse', optimizer='adam')
    return m


def run(args):
    class ValohaiCallback(Callback):
        def on_epoch_end(self, epoch, logs):
            logs["epoch"] = epoch
            print("\n"+json.dumps(logs))

    scaler, train_X, train_y, test_X, test_y = prepare_data(Path(args.data), args.station, args.split, args.num_timesteps)
    train_X_reshaped = train_X.values.reshape(train_X.shape[0], train_X.shape[1], 1)
    test_X_reshaped = test_X.values.reshape(test_X.shape[0], test_X.shape[1], 1)

    callbacks = [ValohaiCallback()]

    model = prepare_model(args.num_timesteps)
    model.fit(train_X_reshaped, train_y, batch_size=512, epochs=20, validation_data=(test_X_reshaped, test_y), shuffle=False, verbose=1, callbacks=callbacks)

    train_predicted = scaler.inverse_transform(model.predict(train_X_reshaped))
    test_predicted = scaler.inverse_transform(model.predict(test_X_reshaped))

    train_RMSE = np.sqrt(mean_squared_error(train_y, train_predicted))
    test_RMSE = np.sqrt(mean_squared_error(test_y, test_predicted))
    return {"RMSE (train)": train_RMSE, "RMSE (test)": test_RMSE}



if __name__== "__main__":
    parser = ArgumentParser()
    parser.add_argument("--data", default="data/")
    parser.add_argument("--results", default="results/")
    parser.add_argument("--station", default="12-PA-SCH")
    parser.add_argument("--split", default="2018-01-01")
    parser.add_argument("--num_timesteps", default=10, type=int)
    args = parser.parse_args()
    results = run(args)
    print(json.dumps(results))


