from sklearn.metrics import mean_squared_error
import numpy as np
from sklearn.linear_model import LinearRegression
from sklearn.ensemble.forest import RandomForestRegressor
import pandas as pd
import matplotlib.pyplot as plt

from features import calculate_features

ONE_WEEK = 7*24


class Baseline:
    def fit(self, X, y):
        return self

    def predict(self, X):
        assert len(X.columns) == 1
        assert X.columns[0] == "lag_one_week"
        return X


def get_model_class(model_name):
    known_models = {"Baseline": Baseline, "LinearRegression": LinearRegression, "RandomForest": RandomForestRegressor}
    if model_name not in known_models:
        raise NotImplementedError()
    return known_models[model_name]


def plot_predictions(actual, predicted, results_dir):
    actual = actual.copy()
    actual.name = "actual"
    predicted = predicted.copy()
    predicted.name = "predicted"

    both = pd.concat([actual, predicted], axis=1)
    both[0:100].plot()
    plt.savefig(results_dir / "predictions.png")


def run_model(model_name, feature_group, counts_train, counts_test, weather_train, weather_test, results_dir, model_params):

    assert counts_train.isnull().sum() == 0
    assert counts_test.isnull().sum() == 0

    train_X = calculate_features(counts_train, weather_train, feature_group)
    train_X = train_X[ONE_WEEK:]  # todo
    train_y = counts_train[ONE_WEEK:]  # todo

    test_X = calculate_features(counts_test, weather_test, feature_group)
    test_X = test_X[ONE_WEEK:]  # todo
    test_y = counts_test[ONE_WEEK:]  # todo

    model = get_model_class(model_name)(**model_params)
    model.fit(train_X, train_y)

    train_predicted = model.predict(train_X)
    test_predicted = model.predict(test_X)

    train_RMSE = np.sqrt(mean_squared_error(train_y, train_predicted))
    test_RMSE = np.sqrt(mean_squared_error(test_y, test_predicted))

    test_predicted = pd.Series(test_predicted, index=test_y.index)
    plot_predictions(test_y, test_predicted, results_dir)

    return {"RMSE (train)": train_RMSE, "RMSE (test)": test_RMSE}

