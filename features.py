import pandas as pd


def _lag_days(counts, num_days):
    return counts.shift(num_days*24)


def _lag_hours(counts, num_hours):
    return counts.shift(num_hours*24)


def lag_one_week(counts, weather):
    return _lag_days(counts, 7)


def lag_one_hour(counts, weather):
    return _lag_hours(counts, 1)


def weekend(counts, weather):
    return pd.Series(counts.index.day_name().isin(["Saturday", "Sunday"]), index=counts.index)


def hour_of_day(counts, weather):
    return pd.Series(counts.index.hour, index=counts.index)


def season(counts, weather):
    feature = pd.Series(None, index=counts.index)
    feature.loc[feature.index.month.isin([12, 1, 2])] = "winter"
    feature.loc[feature.index.month.isin([3, 4, 5])] = "spring"
    feature.loc[feature.index.month.isin([6, 7, 8])] = "summer"
    feature.loc[feature.index.month.isin([9, 10, 11])] = "fall"
    feature = pd.get_dummies(feature, drop_first=False) # todo dropfirst for regression
    return feature


def temperature(counts, weather):
    return weather["apparentTemperature"]


def precipitation(counts, weather):
    return weather["precipIntensity"]


def wind(counts, weather):
    return weather["windSpeed"]


def resolve_feature_group(feature_group):
    lag = ["lag_one_week", "lag_one_hour"]
    date = ["weekend", "hour_of_day", "season"]
    weather = ["temperature", "precipitation", "wind"]

    known_feature_groups = {"all_lag": lag,
                            "all_date": date,
                            "all_weather": weather,
                            "all_lag_date": lag+date,
                            "all_lag_date_weather": lag+date+weather}

    if feature_group not in known_feature_groups:
        return [feature_group]  # is likely not a group but a single feature
    return known_feature_groups[feature_group]


def feature_name_to_method(feature_name):
    known_methods = {"lag_one_week": lag_one_week, "lag_one_hour": lag_one_hour,
                     "weekend": weekend, "hour_of_day": hour_of_day, "season": season,
                     "temperature": temperature, "precipitation": precipitation, "wind": wind}

    if feature_name not in known_methods:
        raise NotImplementedError(feature_name)
    return known_methods[feature_name]


def calculate_features(counts, weather, feature_group):
    feature_names = resolve_feature_group(feature_group)

    features = []
    for name in feature_names:
        method = feature_name_to_method(name)
        feature = method(counts, weather)
        if isinstance(feature, pd.Series):
            feature.name = name
        features.append(feature)
    return pd.concat(features, axis=1)
